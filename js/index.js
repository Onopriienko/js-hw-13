document.getElementById('change-theme').addEventListener('click', themeOptions);

function themeOptions() {
    if (localStorage.getItem('bcg') === 'default'){
        localStorage.setItem("bcg", "hotpink");
        localStorage.setItem("bcg1", "red");
        localStorage.setItem("bcg2", "black");
        window.location.reload();
    }
    else if (localStorage.getItem('bcg') === 'hotpink'){
        localStorage.setItem("bcg", "default");
        localStorage.setItem("bcg1", "default");
        localStorage.setItem("bcg2", "default");
        window.location.reload();
    }
    else{
        localStorage.setItem("bcg", "hotpink");
        localStorage.setItem("bcg1", "red");
        localStorage.setItem("bcg2", "black");
        window.location.reload();
    }
}
function changeColor() {
    document.getElementById("green-btn").style.backgroundColor = localStorage.getItem("bcg");
    document.getElementById("blue-btn").style.backgroundColor = localStorage.getItem("bcg1");
    document.getElementById("main-header").style.backgroundColor = localStorage.getItem("bcg2");
}
if(localStorage.getItem("bcg") === 'hotpink'){
    window.onload = changeColor;
}
else if(localStorage.getItem("bcg") === 'default'){
}
else{
    window.onload = changeColor;
}
